# Synesthesia - Interview Test #2

Create an iOS app based on [TVMaze](https://www.tvmaze.com/api) free APIs displaying a searchable list of TV shows and a custom detail for each one of the results

The app should display a list/grid of elements representing*shows, each one with at least the shows's poster (the main image of the show, under the JSON field `image`).

The list should show a single searchable field, used to search specific shows

When tapping on a show, the detail is shown in a new screen displaying at least:
- Poster
- Show title
- Genres
- Year

UI/UX is at candidate's discretion.

## Main requirements

- Search endpoint: `http://api.tvmaze.com/search/shows?q=lost`
- Deployment target: iOS 11.0
- Use the **MVVM pattern** (Model - View - ViewModel)
- List should be searchable
- Images should be cached
- Cells in list can have fixed height: the `medium` url of each show image in TVMaze should have same aspect ratio.

## Advanced features

- Optimized "search-as-you-type" feature: after typing keyboard, automatically fetch data from API, but only if 1 second has elapsed since user stopped typing
- Use RxSwift as data-binding for MVVM
- Add unit tests with simple mocks to test the networking layer
- Implement a custom user interface for iPad
- Enable screen rotation
- Show description should be HTML escaped
- Cast or seasons horizontal carousel in detail for each show
- When adding custom labels in cells in list (adding show title or list of genres), they should size their height according to text content. Poster image can have fixed size.

## Bonus
- Implement a custom user interface for AppleTV, sharing as much logic and code as possible with the rest of the app

For any technical question regarding this test, contact `stefano.mondino@synesthesia.it`
